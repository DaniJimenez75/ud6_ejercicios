import java.util.Scanner;

/**
 * 
 */

/**
 * @author Dani
 *
 */
public class Ejercicio4App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);
		System.out.print("Introduce numero: ");
		int numero = entrada.nextInt();
		calcularFactorial(numero);//Llamamos al metodo calcularFactorial y le pasamos el numero como parametro
		
	}
	
	//Calculamos numero factorial
	public static void calcularFactorial(int numero) {
		int factorial = 1;
		 for (int i = numero; i > 0; i--) {
             factorial = factorial * i;
         }
		 System.out.print("El numero factorial es: "+factorial);
	}

}
