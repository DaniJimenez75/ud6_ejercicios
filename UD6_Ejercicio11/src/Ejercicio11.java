import java.util.Random;
import java.util.Scanner;

/**
 * 
 */

/**
 * @author Dani
 *
 */
public class Ejercicio11 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);
		System.out.print("Introduce tamaño del los arrays: ");
		int tamañoArray = entrada.nextInt();
		int array1[] = new int[tamañoArray];
		int array2[] = new int[tamañoArray];
		llenarArray(array1);
		llenarArray(array2);
		int arrayMultiplicado[] = multiplicarArrays(array1, array2, tamañoArray);
		System.out.println("------------ARRAY1------------");
		mostrarArray(array1);
		System.out.println("------------ARRAY2------------");
		mostrarArray(array2);
		System.out.println("------------ARRAY MULTIPLICADO ------------");
		mostrarArray(arrayMultiplicado);


	}
	//Metodo para rellenar un array
	public static void llenarArray(int array1[]) {
		for (int i = 0; i < array1.length; i++) {
			int numeroAleatorio = generarNumeroAleatorio();
			array1[i] = numeroAleatorio;
		}
	}

	//Metodo para generar numero aleatorio

	public static int generarNumeroAleatorio() {
		Random numero = new Random();
		int numeroAleatorio = numero.nextInt(10);
		
		return numeroAleatorio;

	}
	
	public static int[] multiplicarArrays(int array1[], int array2[], int tamañoArray) {
		int arrayMultiplicado[] = new int[tamañoArray];//Creamos nuevo array
		for (int i = 0; i < tamañoArray; i++) {
			arrayMultiplicado[i] = array1[i] * array2[i];//Agregamos los resultados al array
		}
		return arrayMultiplicado;
	}
	
	//Metodo para mostrar arrays

	public static void mostrarArray(int array[]) {
		System.out.println("----------------------------------------------");
		System.out.println("Valores array:");
		for (int i = 0; i < array.length; i++) {
			System.out.println("["+i+"] = "+array[i]);
		}
	}

}
