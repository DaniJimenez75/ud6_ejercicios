import java.util.Random;
import java.util.Scanner;

/**
 * 
 */

/**
 * @author Dani
 *
 */
public class Ejercicio2App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);
		System.out.print("Introduce cuantos numeros quieres imprimir:");
		int cantidadNumerosAleatorios = entrada.nextInt();
		System.out.print("Introduce entre que numeros quieres que este:");
		int numero1 = entrada.nextInt();
		System.out.print("Introduce entre que numeros quieres que este:");
		int numero2 = entrada.nextInt();
		System.out.println("NUMEROS ALEATORIOS:");
		for (int i = 0; i < cantidadNumerosAleatorios; i++) {
			int numeroAleatorio = numeroAleatorio(numero1, numero2);
			System.out.println(numeroAleatorio);
		}
		

	}
	
	//Generamos numero aleatorio entre el numero2 y el numero1
	public static int numeroAleatorio(int numero1, int numero2) {
		Random numero = new Random();
		int numeroAleatorio = numero.nextInt(numero2-numero1+1)+numero1;
		return numeroAleatorio;
	}

}
