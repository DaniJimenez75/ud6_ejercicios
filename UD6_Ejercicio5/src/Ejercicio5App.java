import java.util.Scanner;

/**
 * 
 */

/**
 * @author Dani
 *
 */
public class Ejercicio5App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);
		System.out.print("introduce numero: ");
		int numero = entrada.nextInt();
		String binario = pasarABinario(numero);
		System.out.println(binario);

	}
	
	
	public static String pasarABinario(int numero) {
        String binario = "";
        if (numero > 0) {

            while (numero > 0) {
            	
                if (numero % 2 == 0) {//Si el residuo da 0 le agregamos un 0

                    binario = "0" + binario;

                } else {//Si no le agregamos un 1

                    binario = "1" + binario;

                }

                numero = numero / 2;

            }
	}else if (numero == 0) {//Si el numero es 0 el resultado es 0

        binario = "0";
        
}
        return binario;
	}
}


