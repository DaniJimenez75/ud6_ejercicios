import java.util.Random;
import java.util.Scanner;

/**
 * 
 */

/**
 * @author Dani
 *
 */
public class Ejercicio9App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);
		System.out.print("Introduce tama�o del array: ");
		int tama�oArray = entrada.nextInt();
		int array[] = new int[tama�oArray];
		llenarArray(array);
		sumarArray(array);
		

	}
	
	//Metodo para rellenar un array

	public static void llenarArray(int array[]) {
		for (int i = 0; i < array.length; i++) {
			int numeroAleatorio = generarNumeroAleatorio();
			array[i] = numeroAleatorio;
		}
		mostrarArray(array);
	}
	
	//Metodo para generar numero aleatorio
	private static int generarNumeroAleatorio() {
		Random numero = new Random();
		int numeroAleatorio = numero.nextInt(10);
		
		return numeroAleatorio;


	}
	//Metodo para mostrar arrays

	private static void mostrarArray(int array[]) {
		System.out.println("----------------------------------------------");
		System.out.println("Valores array:");
		for (int i = 0; i < array.length; i++) {
			System.out.println("["+i+"] = "+array[i]);
		}
	}
	
	private static void sumarArray(int array[]) {
		System.out.println("----------------------------------------------");
		int sumaArray = 0;//Variable donde almacenamos la suma de todo el array
		for (int i = 0; i < array.length; i++) {
			sumaArray = sumaArray + array[i];
		}
		System.out.println("La suma total de los valores del array es: "+sumaArray);
	}

}
