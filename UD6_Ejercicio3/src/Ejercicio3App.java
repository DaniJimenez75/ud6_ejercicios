import java.util.Scanner;

/**
 * 
 */

/**
 * @author Dani
 *
 */
public class Ejercicio3App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);
		System.out.print("Introduce numero:");
		int numero = entrada.nextInt();
		if(calcularPrimo(numero)) {//Si el metodo devuelve true es primo
			System.out.println("Es Primo");
		}else {
			System.out.println("No es Primo");
		}

	}

		public static boolean calcularPrimo(int numero) {
			boolean EsPrimo = true;						
			for (int i = 2; i < numero-1; i++) {
					if(numero % i == 0) {//Si el numero es divisible por i no es primo
						EsPrimo = false;
					}						
					
				}											
										
			return EsPrimo;
		}
}
