import java.util.Random;
import java.util.Scanner;

/**
 * 
 */

/**
 * @author Dani
 *
 */
public class Ejercicio12 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);
		System.out.print("Introduce tamaño del los arrays: ");
		int tamañoArray = entrada.nextInt();
		int array[] = new int[tamañoArray];
		llenarArray(array);
		mostrarArray(array);
		int arrayNuevo[] = nuevoArray(tamañoArray, array);
		System.out.println("Array acabado con el digito indicado: ");
		mostrarArray(arrayNuevo);


	}
	
	//Metodo para rellenar un array
	public static void llenarArray(int array[]) {
		for (int i = 0; i < array.length; i++) {
			int numeroAleatorio = generarNumeroAleatorio();
			array[i] = numeroAleatorio;
		}
	}
	
	//Metodo para generar numero aleatorio
	public static int generarNumeroAleatorio() {
		Random numero = new Random();
		int numeroAleatorio = numero.nextInt(300)+1;
		
		return numeroAleatorio;

	}
	
	//Metodo para mostrar arrays
	public static void mostrarArray(int array[]) {
		System.out.println("----------------------------------------------");
		System.out.println("Valores array:");
		for (int i = 0; i < array.length; i++) {
			System.out.println("["+i+"] = "+array[i]);
		}
	}
	
	
	public static int[] nuevoArray(int tamañoArray, int array[]) {
		Scanner entrada = new Scanner(System.in);
		int arrayNuevo[] = new int[tamañoArray]; //Creamos nuevo array
		System.out.print("Introduce digito: ");
		int digito = entrada.nextInt(); //Pedimos al usuario digito
		int contadorArrayNuevo = 0; //Posicion donde se almacenara un valor al array nuevo
		while(digito<0 && digito>9) {//Comprobamos digito
			digito = entrada.nextInt();
		}
		
		for (int i = 0; i < array.length; i++) {
			int ultimoDigito = array[i]%10; //Almacenamos el ultimo digito del array[i]
			if(ultimoDigito == digito) {//Si los digitos son iguales se agrega el numero al nuevo array y se suma una posicion al nuevo array
				arrayNuevo[contadorArrayNuevo] = array[i];
				contadorArrayNuevo++;
			}
		}
		
		
		
		
		return arrayNuevo;
	}

}
