import java.util.Random;
import java.util.Scanner;

/**
 * 
 */

/**
 * @author Dani
 *
 */
public class Ejercicio10App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);
		System.out.print("Introduce tama�o del array: ");
		int tama�oArray = entrada.nextInt();
		int array[] = new int[tama�oArray];
		llenarArray(array);
	}
	//Metodo para rellenar un array

	public static void llenarArray(int array[]) {
		Random numero = new Random();
		for (int i = 0; i < array.length; i++) {
			int numeroAleatorio = numero.nextInt(100);
			
			if(comprobarPrimo(numeroAleatorio)) {//Si el metodo retorna true agrega el numero al array
				array[i] = numeroAleatorio;

			}else {
				
				i--;
			}
		}
		mostrarArray(array);
		masGrande(array);
	}
	//Metodo para mostrar arrays

	public static void mostrarArray(int array[]) {
		System.out.println("----------------------------------------------");
		System.out.println("Valores array:");
		for (int i = 0; i < array.length; i++) {
			System.out.println("["+i+"] = "+array[i]);
		}
	}
	
	//Metodo para comprobar si un numero es primo
	public static boolean comprobarPrimo(int numero) {
		boolean EsPrimo = true;						
		for (int i = 2; i < numero-1; i++) {
				if(numero % i == 0) {
					EsPrimo = false;
				}
									
			}																			
		return EsPrimo;
	}
	
	
	public static void masGrande(int array[]) {
		int masGrande = 0;
		System.out.println("----------------------------------------------");
		for (int i = 0; i < array.length; i++) {
			if(masGrande<array[i]) {
				masGrande = array[i];
			}
		}
		
		System.out.println("Valor mas grande dentro del array:"+masGrande);
	}

}
