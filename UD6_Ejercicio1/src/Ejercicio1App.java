import java.util.Scanner;

import javax.swing.JOptionPane;

/**
 * 
 */

/**
 * @author Dani
 *
 */
public class Ejercicio1App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);
		System.out.print("Introduce nombre figura(circulo, cuadrado, triangulo):");
		String figura = entrada.next();
		switch (figura) { //Switch para comprobar figura
		case "circulo":
			System.out.print("Introduce el radio:");
			double radio = entrada.nextInt();	
			double resultadoCirculo = circulo(radio);
			System.out.println("El area del circulo es:"+resultadoCirculo);
			break;
		case "triangulo":
			System.out.print("Introduce la base del triangulo:");
			double base = entrada.nextInt();
			System.out.print("Introduce la altura del triangulo: ");
			double altura = entrada.nextInt();
			double resultadoTriangulo = triangulo(base,altura);
			System.out.println("El area del triangulo es:"+resultadoTriangulo);
			break;
		case "cuadrado":
			System.out.print("Introduce lado del cuadrado:");
			double lado = entrada.nextInt();
			double resultadoCuadrado = cuadrado(lado);
			System.out.println("El area del cuadrado es:"+resultadoCuadrado);
			break;

		default:
			System.out.println("Error al introducir nombre de la figura");
			break;
		}

		

	}
	
	//Calcular area 
	public static double circulo(double radio) {
		double resultado = (Math.pow(radio, 2))*Math.PI;
		return resultado;
		
	}
	
	
	public static double triangulo(double base, double altura) {
		double resultado = (base*altura) / 2;
		return resultado;
		
	}
	
	
	public static double cuadrado(double lado) {
		double resultado = lado * lado;
		return resultado;
		
	}
}
