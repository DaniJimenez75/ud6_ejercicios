import java.util.Scanner;

/**
 * 
 */

/**
 * @author Dani
 *
 */
public class Ejercicio8App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int numeros[] = new int[10];
		llenarArray(numeros);
		

	}
	
	//Metodo para rellenar un array

	public static void llenarArray(int numeros[]) {
		Scanner entrada = new Scanner(System.in);
		for (int i = 0; i < numeros.length; i++) {
			System.out.print("Introduce valor para la posicion ["+i+"]: ");
			int numero = entrada.nextInt();
			numeros[i] = numero;
		}
		mostrarArray(numeros);
	}
	//Metodo para mostrar arrays

	public static void mostrarArray(int numeros[]) {
		System.out.println();
		System.out.println("Valores array:");
		for (int i = 0; i < numeros.length; i++) {
			System.out.println("["+i+"] = "+numeros[i]);
		}
	}

}
