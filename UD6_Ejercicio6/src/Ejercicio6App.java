import java.util.Scanner;

/**
 * 
 */

/**
 * @author Dani
 *
 */
public class Ejercicio6App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);
		System.out.print("introduce numero: ");
		int numero = entrada.nextInt();
		int cifras = contarCifras(numero);
		System.out.println("El numero "+numero+" tiene "+cifras+" cifras");


	}
	
	public static int contarCifras(int numero) {
		int cifras = 0;
		
		while(numero!=0) {
			numero=numero/10;
			cifras++;//Cada vez que dividimos sumamos una cifra hasta que el numero sea igual a 0
		}
		
		return cifras;
	}

}
